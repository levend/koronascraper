var s3 = require('@auth0/s3');

const scrape = require('website-scraper');

function generateDirectory() {

    const dailySaveDirectory = new Date().toISOString()
        .replace('T','-')
        .replace(/\:/g,'_')
        .replace(/\..+/, '');

    return '/tmp/node/' + dailySaveDirectory;
}
async function fetch(rootURL,saveDirectory){

    console.log("Scrape start");

    const dailySaveDirectory = new Date().toISOString()
        .replace('T','-')
        .replace(/\:/g,'_')
        .replace(/\..+/, '');

    const options = {
        urls: [rootURL],
        directory: saveDirectory
    };
    const result = await scrape(options);
    console.log("Scrape end");
}

function upload(sourceDirectory) {
    const promise = new Promise(function(resolve, reject) {
        console.log('Upload to S3 start');
        var client = s3.createClient({

            s3Options: {
                accessKeyId: process.env.S3_ACCESS_KEY,
                secretAccessKey: process.env.S3_ACCESS_SECRET,
                region: process.env.S3_REGION,
            },
        });

        const remoteDirectory = sourceDirectory.substring(sourceDirectory.lastIndexOf('/') + 1);

        var params = {
            localDir: sourceDirectory,

            s3Params: {
                Bucket: process.env.S3_BUCKET_NAME,
                Prefix: remoteDirectory
            },
        };

        let uploader = client.uploadDir(params);
        uploader.on('error', function (err) {
            console.error('unable to sync:', err.stack);
            reject(err);
        });
        uploader.on('progress', function () {
            console.log('progress: ', (uploader.progressAmount / uploader.progressTotal).toPrecision(2));
        });
        uploader.on('end', function () {
            console.log('done uploading');
            resolve();
        });
    });

    return promise;
}

// const URLToScrape = 'https://koronavirus.gov.hu/';
// const saveDirectory = generateDirectory();
//
// fetch(URLToScrape,saveDirectory ).then(()=>{
//     upload(saveDirectory).then(()=>{
//         console.log("Upload completed successfully");
//     }).catch(()=>{
//         console.error("Upload failed");
//     });
// })

exports.handler = async function(event) {
    const promise = new Promise(function(resolve, reject) {

        const URLToScrape = 'https://koronavirus.gov.hu/';
        const saveDirectory = generateDirectory();

        fetch(URLToScrape,saveDirectory ).then(()=>{
            upload(saveDirectory).then(()=>{
                resolve('Upload completed successfully');
            }).catch((error)=>{
                reject(error);
            })

        })
    })
    return promise
}